import Link from 'next/link'

const Header = () => {
    return (
        <header className="fixed flex align-center h-24 w-100pr z-10">
            <div className="container container--atomic-labs">
                <Link href="/">
                    <a>
                        <img className="header-logo" src="/assets/images/layout/logo.png" alt="Atomic Labs Logo" />
                    </a>
                </Link>
            </div>
        </header>
    );
}

export default Header;
