const Footer = () => {
    return (
        <footer className="main-footer bg-black md:tx-20 tx-bold py-10">
            <div className="container container--atomic-labs flex flex-column md:flex-row justify-between md:align-center">
                <div className="mb-8 md:mb-0">
                    &copy; AtomicLabs, Todos los derechos reservados
                </div>

                <div className="flex align-center md:ml-auto">
                    <a className="tx-underline" href="">
                        Aviso de privacidad
                    </a>
                    <div className="flex align-center ml-15">
                        <a className="footer-link mr-7" href="">
                            <img className="icon icon-lnk" src="/assets/images/helpers/icon-linkedin.png" alt="" />
                        </a>
                        <a className="footer-link mt-2" href="">
                            <img className="icon icon-tw" src="/assets/images/helpers/icon-twitter.png" alt="" />
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;
