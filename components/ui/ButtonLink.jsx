import Link from 'next/link'

const ButtonLink = ({ title, path }) => {
    return (
        <Link href={ path ? path : '/' }>
            <a className="btn btn--white flex w-100pr mx-auto">
                { title }
            </a>
        </Link>
    );
}

export default ButtonLink;

