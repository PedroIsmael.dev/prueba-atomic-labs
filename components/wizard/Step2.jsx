const Step1 = (props) => {
    if ( props.currentStep !== 2 ) {
        return null
    }
    return (
        <div>
            <h1 className="step-title mb-18">
                <img className="icon" src="/assets/images/helpers/numbers/2.png" alt="" />
                <span>
                    Valida tu&nbsp;<span className="tx-primary-1"> celular</span>
                </span>
            </h1>

            <p className="tx-20 xl:tx-24 tx-medium mb-2">
                Necesitamos validar tu numero para continuar
            </p>
            <p className="tx-18 xl:tx-20 tx-medium mb-12">
                Ingresa tu número a 10 dígitos y te enviaremos un código SMS
            </p>

            {/* PHONE */}
            <div className="form-group mb-12" style={{ maxWidth: 444 }}>
                <label className="form-label" htmlFor="">
                    Número Celular
                </label>
                <input className="form-input" type="number" name="userPhoneNumber" id="" value={ props.userPhoneNumber } onChange={ props.handleChange } maxLength="10"/>
            </div>

            {/* SUBMIT */}
            <button className={"btn btn--primary-1 flex ml-auto" +
                (props.userPhoneNumber === null ? " opacity-60 no-events" : "")} onClick={ props.handleStep } onClick={ props.handleStep } onClick={ props.handleStep }>
                Continuar
            </button>
        </div>
    );
}

export default Step1;
