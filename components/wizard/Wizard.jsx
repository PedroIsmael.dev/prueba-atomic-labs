import { useState } from 'react'
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import Step4 from './Step4';

const Wizard = () => {
    const [currentStep, setcurrentStep] = useState(1);
    const [progressBar, setprogressBar] = useState(13);
    const [form, setform] = useState({
        userName: '',
        userLastName: '',
        userPhoneNumber: null,
        userCode: '',
        userTerms: false
    });

    const validateStep1 = () => {
        let error = false
        if ( form.userName === '' ) error = true
        if ( form.userLastName === '' ) error = true
        return error;
    }

    const validateStep2 = () => {
        let error = false
        if ( form.userPhoneNumber === null ) error = true
        return error;
    }

    const validateStep3 = () => {
        let error = false
        if ( form.userCode === '' ) error = true
        return error;
    }

    const validateStep4 = () => {
        let error = false
        if ( form.userTerms === false ) error = true
        return error;
    }

    const handleChange = event => {
        const { name, value } = event.target
        setform({
            ...form,
            [name] : value
        })
    }
    
    const handleStep = () => {
        if ( currentStep === 1 ) if (validateStep1() ) { return }
        if ( currentStep === 2 ) if (validateStep2() ) { return }
        if ( currentStep === 3 ) if (validateStep3() ) { return }
        if ( currentStep === 4 ) if (validateStep4() ) { return }
        
        let _currentStep = currentStep
        _currentStep = _currentStep + 1
        if (_currentStep === 2) setprogressBar(38)
        if (_currentStep === 3) setprogressBar(63)
        if (_currentStep === 4) setprogressBar(87)

        setcurrentStep(_currentStep)
    }

    const prevStep = () => {
        let _currentStep = currentStep
        _currentStep = _currentStep <= 1 ? 1 : _currentStep - 1
        if (_currentStep === 1) setprogressBar(13)
        if (_currentStep === 2) setprogressBar(38)
        if (_currentStep === 3) setprogressBar(63)
        if (_currentStep === 4) setprogressBar(87)

        setcurrentStep(_currentStep)
    }

    return (
        <div className="wizard relative z-0 pb-20">
            <div className="wizard-steps mb-24">

                {/* 1 */}
                <div className="step">
                    <img className="step-icon" src={ currentStep == 1 ? "/assets/images/helpers/numbers/1.png" : "/assets/images/helpers/icon-check.png" } alt="" />
                </div>

                {/* 2 */}
                <div className="step">
                    <img className="step-icon" 
                        src={ currentStep == 2 ? "/assets/images/helpers/numbers/2.png" :
                              currentStep > 2 ? "/assets/images/helpers/icon-check.png" : 
                              "/assets/images/helpers/numbers/2-outline-grey.png"} alt="" 
                    />
                </div>

                {/* 3 */}
                <div className="step">
                    <img className="step-icon" 
                        src={ currentStep == 3 ? "/assets/images/helpers/numbers/3.png" :
                              currentStep > 3 ? "/assets/images/helpers/icon-check.png" : 
                              "/assets/images/helpers/numbers/3-outline-grey.png"} alt="" 
                    />
                </div>

                {/* 4 */}
                <div className="step">
                    <img className="step-icon" 
                        src={ currentStep == 4 ? "/assets/images/helpers/numbers/4.png" :
                              currentStep > 4 ? "/assets/images/helpers/icon-check.png" : 
                              "/assets/images/helpers/numbers/4-outline-grey.png"} alt="" 
                    />
                </div>

                {/* PROGRESS BAR */}
                <div className="wizard-progress">
                    <div className="wizard-progress-bar" style={{ width: progressBar+'%' }}></div>
                </div>
            </div>

            {   currentStep !== 1 &&
                <div className="tx-18 tx-medium mb-8 cursor-pointer w-fit hover:tx-primary-1" onClick={prevStep}>
                    {'< Regresar'}
                </div>
            }

            <Step1 
                currentStep={ currentStep }
                userName={ form.userName } 
                userLastName={ form.userLastName } 
                handleChange={ handleChange }
                handleStep={ handleStep }
            />

            <Step2 
                currentStep={ currentStep }
                userPhoneNumber={ form.userPhoneNumber } 
                handleChange={ handleChange }
                handleStep={ handleStep }
            />

            <Step3 
                userPhoneNumber={ form.userPhoneNumber }
                currentStep={ currentStep }
                userCode={ form.userCode } 
                handleChange={ handleChange }
                handleStep={ handleStep }
            />

            <Step4 
                currentStep={ currentStep }
                handleChange={ handleChange }
                handleStep={ handleStep }
            />

            <img className="sample-wizard absolute t-0 r-0 -z-1" src="/assets/images/layout/sample-wizard.png" alt="" />
        </div>
    );
}

export default Wizard;
