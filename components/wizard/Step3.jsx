const Step1 = (props) => {
    if ( props.currentStep !== 3 ) {
        return null
    }
    return (
        <div>
            <h1 className="step-title mb-10">
                <img className="icon" src="/assets/images/helpers/numbers/3.png" alt="" />
                <span>
                    Código de&nbsp;<span className="tx-primary-1"> verificación</span>
                </span>
            </h1>

            <p className="tx-20 xl:tx-24 tx-medium mb-6">
                Te enviamos un SMS al número: <br />
                { props.userPhoneNumber }
            </p>
            <p className="tx-18 xl:tx-20 tx-medium mb-12">
                Ingresa el código de verificación
            </p>

            {/* CODE */}
            <div className="form-group mb-12" style={{ maxWidth: 444 }}>
                <label className="form-label" htmlFor="">
                    Código de verificación
                </label>
                <input className="form-input" type="number" name="userCode" id="" value={ props.userCode } onChange={ props.handleChange }/>
            </div>

            <p className="xl:tx-22 mb-6">
                ¿No recibiste el código? <strong> Reenviar código </strong>
            </p>

            {/* SUBMIT */}
            <button className={"btn btn--primary-1 flex ml-auto" +
                (props.userCode === '' ? " opacity-60 no-events" : "")} onClick={ props.handleStep }>
                Validar código
            </button>
        </div>
    );
}

export default Step1;
