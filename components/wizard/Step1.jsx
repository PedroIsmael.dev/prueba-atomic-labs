const Step1 = (props) => {
    if ( props.currentStep !== 1 ) {
        return null
    }
    return (
        <div>
            <h1 className="step-title mb-18">
                <img className="icon" src="/assets/images/helpers/numbers/1.png" alt="" />
                <span>
                    Te queremos&nbsp;<span className="tx-primary-1"> conocer</span>
                </span>
            </h1>

            <p className="tx-20 xl:tx-24 tx-medium mb-12">
                Queremos saber que eres tu porfavor ingresa los siguientes datos
            </p>

            {/* NAME */}
            <div className="form-group mb-8" style={{ maxWidth: 444 }}>
                <label className="form-label" htmlFor="">
                    Nombres(s)
                </label>
                <input className="form-input" type="text" name="userName" id="" value={ props.userName } onChange={ props.handleChange }/>
            </div>

            {/* LAST NAME */}
            <div className="form-group mb-8" style={{ maxWidth: 444 }}>
                <label className="form-label" htmlFor="">
                    Apellido
                </label>
                <input className="form-input" type="text" name="userLastName" id="" value={ props.userLastName } onChange={ props.handleChange }/>
            </div>

            {/* SUBMIT */}
            <button className={"btn btn--primary-1 flex ml-auto" +
                (props.userName === '' || props.userLastName === '' ? " opacity-60 no-events" : "")} onClick={ props.handleStep } onClick={ props.handleStep }>
                Enviar
            </button>
        </div>
    );
}

export default Step1;
