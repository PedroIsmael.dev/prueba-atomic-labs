const Step1 = (props) => {
    if ( props.currentStep !== 4 ) {
        return null
    }
    return (
        <div>
            <h1 className="step-title mb-18">
                <img className="icon" src="/assets/images/helpers/numbers/4.png" alt="" />
                <span>
                    Términos y&nbsp;<span className="tx-primary-1"> condiciones</span>
                </span>
            </h1>

            <p className="tx-20 xl:tx-24 tx-medium mb-8 md:mb-2">
                Por favor revisa nuestro terminos y condiciones para este servicio
            </p>
            <p className="tx-16 tx-bold tx-underline mb-12">
                Consulta terminos y condiciones
            </p>

            <div className="form-check mb-10">
                <input className="form-check-input" type="checkbox" id="test" name="userTerms" value={ props.userTerms } onChange={ props.handleChange }/>
                <label className="form-check-label" htmlFor="test">
                    Acepto los terminos y condiciones
                </label>
            </div>

            {/* SUBMIT */}
            <button className="btn btn--primary-1 flex ml-auto" onClick={ props.handleStep }>
                Continuar
            </button>
        </div>
    );
}

export default Step1;
