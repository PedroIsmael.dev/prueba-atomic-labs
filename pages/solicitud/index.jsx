import Footer from '../../components/ui/Footer'
import Header from '../../components/ui/Header'
import Wizard from '../../components/wizard/Wizard';

const Index = () => {
    return (
        <main className="overflow-hidden">
            {/* HEADER */}
            <Header/>

            <section className="main-bg bg-b bg-cover bg-no-repeat min-h-100vh pt-40">
                <div className="container container--atomic-labs">
                    <Wizard/>
                </div>
            </section>

            {/* FOOTER */}
            <Footer/>
        </main>
    );
}

export default Index;
