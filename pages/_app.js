import '../styles/static/plumcss/plumcss.css'
import styles from '../styles/scss/app.scss'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
