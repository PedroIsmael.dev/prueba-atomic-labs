import ButtonLink from '../components/ui/ButtonLink';
import Footer from '../components/ui/Footer'
import Header from '../components/ui/Header'

export default function Home() {
    return (
        <main className="content-home bg-secondary-1 min-h-100vh">
            {/* HEADER */}
            <Header/>

            {/* HERO */}
            <section className="main-bg flex align-end md:align-center lg:align-end bg-center bg-cover bg-no-repeat bg-b min-h-100vh">
                <div className="container container--atomic-labs">
                    <div className="gs:grid align-center mb-10 md:mb-20">
                        <div className="col-24 md:col-12 mb-10 md:mb-0">
                            <div className="px-10 sm:px-30 md:px-0">
                                <img src="/assets/images/pages/home/sample-hero.png" alt="sample 1"/>
                            </div>
                        </div>

                        <div className="col-24 md:col-12">
                            <p className="tx-32 xxl:tx-60 tx-center md:tex-left tx-bold mb-8 lg:mb-14">
                                Desarrolla todo <br />
                                <span className="tx-primary-1">
                                    tu POTENCIAL
                                </span> <br />
                                dentro del equipo <br />
                                <span className="tx-44 xxl:tx-90">
                                    <span className="tx-primary-1">
                                        ATOMIC
                                    </span>LABS
                                </span>
                            </p>

                            <ButtonLink title="¡Quiero ser parte!" path="/solicitud"/>
                        </div>
                    </div>

                    {/* SEE MORE */}
                    <a className="flex flex-column align-center w-fit mx-auto mb-10 cursor-pointer" href="#section2">
                        <img className="mb-1 md:mb-4" width="60" src="/assets/images/helpers/icon-arrow-bottom-circle.png" alt="Icon arrow down" />
                        <div className="tx-20">
                            Quiero saber más
                        </div>
                    </a>
                </div>
            </section>

            {/* SECTION 2 */}
            <section id="section2" className="section-2 min-h-100vh pt-30">
                <div className="container container--atomic-labs">
                    <h2 className="section-title tx-center mb-25">
                        Somos el brazo derecho <br />
                        <span className="tx-primary-1">
                            de la tecnología
                        </span>
                    </h2>

                    <div className="wrapper-boxes flex flex-column md:flex-row relative align-center justify-between xl:mx-auto px-5 md:px-0">
                        <div className="box box--white mb-10 md:mb-0">
                            <img className="block mx-auto" src="/assets/images/pages/home/icon-explore.png" alt="" />
                            <hr className="border-b-3 border-primary-1 mx-auto" style={{ maxWidth: 120 }} />

                            <div className="tx-primary-1 tx-28 xl:tx-34 tx-bold tx-center mb-10">
                                EXPLORA
                            </div>

                            <ul className="tx-18 xl:tx-24 tx-dark-1 spacing-0">
                                <li>
                                    Innovacion y <span className="tx-bold">creacion de tecnologia </span>
                                </li>
                                <li className="tx-bold">
                                    UI / UX
                                </li>
                                <li className="tx-bold">
                                    Innovacion
                                </li>
                            </ul>
                        </div>

                        <div className="box box--orange xl:absolute mx-auto l-0 r-0 mb-10 md:mb-0">
                            <img className="block mx-auto" src="/assets/images/pages/home/icon-imagine.png" alt="" />
                            <hr className="border-b-3 border-white mx-auto" style={{ maxWidth: 120 }} />

                            <div className="tx-28 xl:tx-34 tx-bold tx-center mb-10">
                                    IMAGINA
                            </div>

                            <ul className="tx-18 xl:tx-24 spacing-0">
                                <li>
                                    <span className="tx-bold">Estrategia </span> digital
                                </li>
                                <li>
                                    Big Data & <span className="tx-bold"> Analysis </span>
                                </li>
                                <li>
                                    <span className="tx-bold">Consultoría </span>Tecnológica
                                </li>
                                <li>
                                    <span className="tx-bold">Reducción</span> de costos TI
                                </li>
                            </ul>
                        </div>

                        <div className="box box--white">
                            <img className="block mx-auto" src="/assets/images/pages/home/icon-conquest.png" alt="" />
                            <hr className="border-b-3 border-primary-1 mx-auto" style={{ maxWidth: 120 }} />

                            <div className="tx-primary-1 tx-28 xl:tx-34 tx-bold tx-center mb-10">
                                CONQUISTA
                            </div>

                            <ul className="tx-18 xl:tx-24 tx-dark-1 spacing-0">
                                <li>
                                    Desarrollo tecnológico<span className="tx-bold"> a la medida</span>
                                </li>
                                <li className="tx-bold">
                                    Ciberseguridad
                                </li>
                                <li className="tx-bold">
                                    Servicios de la nube
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            {/* SECTION 3 */}
            <section className="section-3 min-h-100vh pt-40 md:pt-20">
                <div className="container container--atomic-labs">
                    <h2 className="section-title tx-center mb-20">
                        ¡Te encantará<br />
                        <span className="tx-primary-1">
                            trabajar con nosotros!
                        </span>
                    </h2>

                    <img className="block mx-auto mb-15" src="/assets/images/pages/home/sample-section-3.png" alt="" />

                    {/* STEPS */}
                    <div className="tx-10 md:tx-22 tx-medium flex justify-between align-center tx-center mb-15">
                        <span>Contratación <br />remota </span>
                        <span className="tx-primary-1 tx-22 xl:tx-40">&rarr;</span>
                        <span>Entrevista con <br />el área de RH</span>
                        <span className="tx-primary-1 tx-22 xl:tx-40">&rarr;</span>
                        <span>Prueba<br />práctica</span>
                        <span className="tx-primary-1 tx-22 xl:tx-40">&rarr;</span>
                        <span>Entrevista<br />técnica</span>
                    </div>
                    <ButtonLink title="¡Quiero ser parte!" path="/solicitud"/>
                </div>
            </section>

            {/* SECTION 4 */}
            <section className="section-4 min-h-100vh pt-30">
                <div className="container container--atomic-labs">
                    <h2 className="section-title tx-center mb-70">
                        ¿Por qué<span className="tx-primary-1"> atomic?</span>
                    </h2>                    
                </div>

                <div className="contain-blue bg-secondary-2 xxl:px-30 pt pb-20">
                    <div className="container container--atomic-labs -mt-50">
                        <div className="section-4-grid tx-center md:tx-left mb-30">

                            {/* ITEM */}
                            <div className="item">
                                <img className="item-img mb-8" src="/assets/images/pages/home/icon-modern.png" alt="" />

                                <ul className="list-info spacing-0 bullet-none">
                                    <li className="list-info-item">
                                        <img className="bullet" src="/assets/images/helpers/icon-check-outline.png" alt="" />
                                        <span>
                                            Usamos las tecnologias mas modernas
                                        </span>
                                    </li>
                                    <li className="list-info-item">
                                        <img className="bullet" src="/assets/images/helpers/icon-check-outline.png" alt="" />
                                        <span>
                                            Innovamos y creamos proyectos retadores
                                        </span>
                                    </li>
                                </ul>
                            </div>

                            {/* ITEM */}
                            <div className="item">
                                <img className="item-img mb-8" src="/assets/images/pages/home/icon-worker.png" alt="" />

                                <ul className="list-info spacing-0 bullet-none">
                                    <li className="list-info-item">
                                        <img className="bullet" src="/assets/images/helpers/icon-check-outline.png" alt="" />
                                        <span>
                                            ¡Trabajamos en equipo rumbo al éxito!
                                        </span>
                                    </li>
                                    <li className="list-info-item">
                                        <img className="bullet" src="/assets/images/helpers/icon-check-outline.png" alt="" />
                                        <span>
                                            No tenemos código de bestimenta
                                        </span>
                                    </li>
                                </ul>
                            </div>

                            {/* ITEM */}
                            <div className="item">
                                <img className="item-img mb-8" src="/assets/images/pages/home/icon-dance.png" alt="" />

                                <ul className="list-info spacing-0 bullet-none">
                                    <li className="list-info-item">
                                        <img className="bullet" src="/assets/images/helpers/icon-check-outline.png" alt="" />
                                        <span>
                                            Realizamos actividades para tu bienestar
                                        </span>
                                    </li>
                                    <li className="list-info-item">
                                        <img className="bullet" src="/assets/images/helpers/icon-check-outline.png" alt="" />
                                        <span>
                                            !Tenemos un parque frente a la oficina!
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="box box--white px-10 mb-20 overflow-auto">
                            <table className="atomic-table tx-dark-1 w-100pr">
                                <thead className="thead">
                                    <tr className="row">
                                        <th className="heading">
                                            Características
                                        </th>
                                        <th className="heading">
                                            Otros
                                        </th>
                                        <th className="heading">
                                            <span className="xxl:tx-36">
                                                Atomic
                                            </span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody className="tbody">
                                    <tr className="row">
                                        <td className="cell">
                                            Equipo inclusivo, honesto y autético
                                        </td>
                                        <td className="cell">
                                            <img className="icon-check" src="/assets/images/helpers/icon-check-outline.png" alt="Icon check" />
                                        </td>
                                        <td className="cell">
                                            <img className="icon-check" src="/assets/images/helpers/icon-check-outline.png" alt="Icon check" />
                                        </td>
                                    </tr>
                                    <tr className="row">
                                        <td className="cell">
                                            Puntualidad es nuestro segundo nombre
                                        </td>
                                        <td className="cell">
                                            <img className="icon-check" src="/assets/images/helpers/icon-check-outline.png" alt="Icon check" />
                                        </td>
                                        <td className="cell">
                                            <img className="icon-check" src="/assets/images/helpers/icon-check-outline.png" alt="Icon check" />
                                        </td>
                                    </tr>
                                    <tr className="row">
                                        <td className="cell">
                                            Siempre innovamos en nuestro productos
                                        </td>
                                        <td className="cell">
                                            <img className="icon-check" src="/assets/images/helpers/icon-check-outline.png" alt="Icon check" />
                                        </td>
                                        <td className="cell">
                                            <img className="icon-check" src="/assets/images/helpers/icon-check-outline.png" alt="Icon check" />
                                        </td>
                                    </tr>
                                    <tr className="row">
                                        <td className="cell">
                                            Te ayudamos a crecer e implementar nuevos conocimientos
                                        </td>
                                        <td className="cell"></td>
                                        <td className="cell">
                                            <img className="icon-check" src="/assets/images/helpers/icon-check-outline.png" alt="Icon check" />
                                        </td>
                                    </tr>
                                    <tr className="row">
                                        <td className="cell">
                                            Nos preocupamos por tu bienestar
                                        </td>
                                        <td className="cell"></td>
                                        <td className="cell">
                                            <img className="icon-check" src="/assets/images/helpers/icon-check-outline.png" alt="Icon check" />
                                        </td>
                                    </tr>
                                    <tr className="row">
                                        <td className="cell">
                                            El respeto es una parte fundamental
                                        </td>
                                        <td className="cell"></td>
                                        <td className="cell">
                                            <img className="icon-check" src="/assets/images/helpers/icon-check-outline.png" alt="Icon check" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <ButtonLink title="¡Quiero ser parte!" path="/solicitud"/>
                    </div>
                </div>
            </section>

            {/* FOOTER */}
            <Footer/>
        </main>
    )
}
